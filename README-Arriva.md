# ARRIVA
	######## VERSION HISTORY ###########
# v.3.6 -- ANDRY BERMAN -- Jun 28, 2019
	# - based on v.3.5 configuration
	# - added frequency-mode=regulatory-domain (default value) in /interface wireless this is new paramater in ROS 6.43.15
	# - Compatibility RB912UAG-2HPnD ROS v6.43.15/user manager v6.43.15
# v.3.5 -- ANDRY BERMAN -- Jan 16, 2019
	# - based on v.3.4 configuration
	# - Compatibility RB912UAG-2HPnD ROS v6.42.9/user manager v6.42.9 Modem LTE Huawei ME909s-821
	# - Usb Power Reset added for LTE modem will execute when the LTE modem status not "bound"
# v.3.4 -- ANDRY BERMAN -- Oct 10, 2018
	# - FW upgrade removed
	# - Compatibility RB912UAG-2HPnD ROS v6.40.9/user manager v6.40.9 Modem LTE Huawei ME909s-821
	# - changed format syntax from trial-uptime=2h/10s become format syntax trial-uptime-limit=2h trial-uptime-reset=10s
# v3.3 -- WILLY MOOREN -- Mai 10, 2017
	# - FW upgrade added
# v3.2 -- TOM RAVEN -- Jul 27, 2016
	# - NTP client IP set to 80.94.65.10
	# - Usermanager:
	#	- Clear-Log added/ modified
	#	- Database clear added/ modified
	#	- Create session added/ modified
	# - Obsolete scripts and schedulers removed:
	#	- Clearit
	#	- Clearppp
	# - VPN IP set to 95.128.6.49
	# - WLAN DHCP Lease time set to 1h
	# - rx-chains, tx-chains set to 1
	# - WLAN country set to Netherlands
# v3.1 -- HERRY DARMAWAN -- Dec 22, 2015
	# - Compatibility with Modem ME909s-821
	# - Modify MASQ rule to accept LTE or PPP-OUT
	# - Acivate both the Tx-Chain and Rx-Chain for wlan1
	# - Clear User-Manager contents before adding 
	# - New standard based on v6.33.3
# v3.0 -- HERRY DARMAWAN --- Nov 27, 2015
	# - Checking for 4G or 3G
	# - Use SYSTEM IDENTITY for the BUSID
	# - Checking for UPDATE or NEW
	# - Checking for User-Manager Package
	# Rev1 - Fixed the changes in the User manager (/tool user-manager user add username instead of user)
# v2.2-4G - HERRY DARMAWAN --- Oct 15, 2015
	# 1. Use 4G instead of 3G
# v2.2 - HERRY DARMAWAN
	# 1. Add information for RADIUS based Winbox Login
	# 2. Add additional routing to RADIUS (10.255.254.0/24)
	# 3. Change the L2TP destination to 95.128.6.51 (new RB for ARRIVA Bus only)
# v2.1 - HERRY DARMAWAN
	# 1. Standarized v6 and v5
	# 2. Add User-Manager and RADIUS settings
	# 3. Add Detection for USB PORT
	# 4. Change L2TP name from 'test' to 'tunnel'
# v2.0 - HERRY DARMAWAN
	# 1. Structured COMMAND (grouped based on the feature added)
	# 2. Set a log lines after every successfull attempt
